import express, { json } from "express"
import { firstRouter } from "./src/route/firstRouter.js";
import { traineesRouter } from "./src/route/traineesRouter.js";
import { schoolsRouter } from "./src/route/schoolsRouter.js";
import { connectToMongoDb } from "./src/connectToDb/connectToMongoDb.js";
import { studentRouter } from "./src/route/studentRouter.js";
import { collegeRouter } from "./src/route/collegeRouter.js";
import { contactRouter } from "./src/route/contactRouter.js";
import { classRoomRouter } from "./src/route/classRoomRouter.js";
import { productRouter } from "./src/route/productRouter.js";
import { userRouter } from "./src/route/userRouter.js";
import bcrypt from "bcrypt"
import { reviewRouter } from "./src/route/reviewRouter.js";
import jwt from "jsonwebtoken"
import { config } from "dotenv";
import { port } from "./src/constan.js";
import cors from "cors"
import { webUserRouter } from "./src/route/webUserRouter.js";

config()
let expressApp = express()
expressApp.use(json())
expressApp.use(cors())
connectToMongoDb()

//application middleware

// expressApp.use((req, res , next) => {
//     console.log('This is a global middleware')
//     next();
// })

expressApp.use("/bike",firstRouter)
expressApp.use('/trainees',traineesRouter)
expressApp.use('/schools',schoolsRouter)
expressApp.use('/students', studentRouter)
expressApp.use('/colleges', collegeRouter)
expressApp.use('/contacts', contactRouter)
expressApp.use('/classrooms', classRoomRouter)
expressApp.use('/reviews', reviewRouter)
expressApp.use('/products',productRouter)
expressApp.use('/users',userRouter)
expressApp.use('/webUsers',webUserRouter)



expressApp.listen(port , ()=>{
    console.log("app is listening at port 8000")
}
)

// let password = "abc@1234"

// let hasPassword = await bcrypt.hash(password, 10)
// console.log(hasPassword)

// let hashPassword = "$2b$10$.syFb3xZJw1d6/9A/B/OQOOZV.GosLCnzaoXXOwcKSsjFJLTzMto2"

// let password = "abc@1234"
// let isPasswordMatch = await bcrypt.compare(password, hashPassword)
// console.log(isPasswordMatch)

// 
// let token = jwt.sign(infoObj, secretKey, expirifyInfo)
// console.log(token)

// let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiamVuaXNoIiwiYWdlIjoyMSwiaWF0IjoxNzA3MDY2ODIyLCJleHAiOjE3Mzg2MDI4MjJ9.WZ7nhKQ-wHz7sCabdy-OsLHdiduyyYZGhEf5T39f03Y"

// try {
//     let inObj = jwt.verify(token, "dw11")
//     console.log(inObj)
// } catch (error) {
//     console.log(error.message)
// }

