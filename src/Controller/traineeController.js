import { Trainee } from "../schema/model.js "

export let postTrainee = async(req,res,next) => {
    let data = req.body

    try {
    let result = await Trainee.create(data)
    res.json({success:true, message:"trainees created successfully",
    result:result 
})
      
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}
export let getTrainee = async(req,res,next) => {

    try {
    let result = await Trainee.find({})
    res.json({success:true, 
        message:"trainees read successfully",
        result:result
    })
    
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }  
}

export let getSpecificTrainee = async(req, res, next)=> {
    let id = req.params.id
    try {
        let result = await Trainee.findById(id)
        res.json({"success": true,message:"Trainees  read successfully",
        result:result
    })
        
    } catch (error) {
        res.json({
            success:true,
            message:error.message
        })
    }
}

export let updateTrainee = async(req,res,next) => {
    let id = req.params.id
    let data = req.body
    try {
    let result = await Trainee.findByIdAndUpdate(id, data, {new:true})
    res.json({success:true,
         message:"trainees updated successfully",
         result:result
        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteTrainee = async(req,res,next) => {
    let id = req.params.id
    try {
    let result = await Trainee.findByIdAndDelete(id)
    
    if (result === null){
        res.json({
            success:false,
            message:"student does ont exits",
        })
    }
    else{
        res.json({success:true, 
            message:"Trainee deleted successfully",
            result:result
        })
    }
    
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
}