import { Review } from "../schema/model.js"

export let postReview = async(req,res,next) => {
    let data =  req.body

    try {
    let result = await Review.create(data)
    res.json({success:true, message:"department create successfully",
    result:result
})

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
}

export let getReview = async(req,res,next) => {

    try {
    let result = await Review.find({}).populate("productID").populate("userID")
    res.json({
        success:true,
        message:"department read successfully",
        result: result,
    })
       
    } catch (error) {
        res.json({
            success:false,
            message:"Unable to read departments",
        })
        
    }
}

export let getSpecificReview = async(req,res,next) => {

    let id = req.params.id
    try {
        let result = await Review.findById(id)
        res.json({success:true, message:"Department Read successfully",
        result: result,
    
    })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
}

 export let updateReview = async(req,res,next) => {
    let id = req.params.id
    let data = req.body

    try {
    let result = await Review.findByIdAndUpdate(id, data, { new: true })
    res.json({success:true, 
        message:"department Updated successfully",
        result:result
    })

        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteReview = async(req,res,next) => {
    let id = req.params.id
    
    try {
        let result = await Review.findByIdAndDelete(id)
        
        if (result === null){
            res.json({
                success:false,
                message:"Department does ont exits",
            })
        }
        else{
            res.json({success:true, 
                message:"Department deleted successfully",
                result:result
            })
        }
        
        } catch (error) {
            res.json({
                success:false,
                message:error.message
            })
            
        }
}