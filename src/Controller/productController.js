import { Product } from "../schema/model.js"

export let postProduct = async(req,res,next)=>{
    let data = req.body

    try {
    let result = await Product.create(data)
    res.json({
        success: true, 
        messageL: "Contact created successfully"       
    })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
}

export let getProduct = async(req,res,next) => {
    let limit = req.query.limit
    let page = req.query.page
    try {
    let result = await Product.find({})
    res.json({
        success:true,
        message:"Contact read successfully",
        result:result
    })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
   
}

export let getSpecificProduct = async(req,res,next) => {
    let id = req.params.id
    try {
        let result = await Product.findById(id)
        res.json({
            success:true,
            message:"Contact is read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateProduct = async(req,res,next) => {
    let id = req.params.id
    let data = req.body

    try {
        let result = await Product.findByIdAndUpdate(id,data, {new:true})
        res.json({
            success:true,
            message:"Contact updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteProduct = async(req,res,next) => {
    let id = req.params.id
    try {
    let result = await Product.findByIdAndDelete(id)
    
    if (result === null){
        res.json({
            success:false,
            message:"contact does ont exits",
        })
    }
    else{
        res.json({success:true, 
            message:"Contact deleted successfully",
            result:result
        })
    }
    
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
}