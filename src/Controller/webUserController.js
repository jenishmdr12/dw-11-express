// import { WebUser } from "../schema/model.js"
import bcrypt from "bcrypt"
// import { sendEmail } from "../utils/sendMail.js"

import { WebUser } from "../schema/model.js"
import jwt from "jsonwebtoken"
import { sendEmail } from "../utils/sendMail.js"
import { secretKey, user } from "../constan.js"
import { json } from "express"
// import { sendEmail } from "../utils/sendMail.js"

export const createWebUser = async (req,res) => {

try {
    let data = req.body
    let hashPassword = await bcrypt.hash(data.password, 10)

    data ={
        ...data,
        isVerifiedEmail:false,
        password:hashPassword
    }
    let result = await  WebUser.create(data)
    
    let infoObj = {
        _id:result._id,
    }
    let expiryInfo = {
        expiresIn:"5d",
    }
    console.log(secretKey)
    let token = await jwt.sign(infoObj, secretKey, expiryInfo)
    console.log(token)

    sendEmail({
        from: "'hello'<jenishmdhr@gmail.com>",
        to: data.email,
        subject: `account create`,
        html: `
      <h1>Your account has been created.</h1>
      <a href="http://localhost:3000/verify-email?token=${token}">
      http://localhost:3000/verify-email?token=${token}
      </a>
      `,
      });


    res.json({
        success:true,
        message:"user created successgully",
        data:result
    })
} catch (error) {
    res.json({
        success:false,
        message:error.message
    })
}
}

export const verifyEmail = async (req,res,next)=>{

    try {

    let tokenString = req.headers.authorization
    let tokenArray = tokenString.split(" ")
    let token = tokenArray[1]
    console.log(token)

    console.log(secretKey)

    let infoObj = await jwt.verify(token, secretKey)
    console.log("******",infoObj)
    let userId = infoObj._id

    let result = await WebUser.findByIdAndUpdate(userId,  {
        isVerifiedEmail:true
    },{
        new:true
    })

    res.json({
        success:true,
        message:"user verified successfully",
        data:result
    })
    } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
    }
}

export const loginUser = async (req, res, next) => {
    try {
        let email = req.body.email
        let password = req.body.password

        let user = await WebUser.findOne({email:email})

        if(user)
        {
            if(user.isVerifiedEmail)
            {
                let isValidPassword = await bcrypt.compare(password, user.password)
                if(isValidPassword)
                {
                    let infoObj = {
                        _id: user._id,
                    }
                    let secretKey = "express6"
                    let expiryInfo = {
                        expiresIn:"5d",
                    }
                    let token = await jwt.sign(infoObj, secretKey, expiryInfo)

                    res.json({
                        success:true,
                        message:"user login successful",
                        data:token
                    })
                }
                else{
                    let error = new Error("crediental does not match")
                    throw error
                }
            }
            else{
                let error = new Error("credential does not match")
                throw error
            }
        }
        else{
            let error = new Error("credential not found")
            throw error
        }
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let getWebUser = async(req,res,next) => {
    let limit = req.query.limit
    let page = req.query.page
    try {
    let result = await WebUser.find({})
    res.json({
        success:true,
        message:"WebUser read successfully",
        result:result
    })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
   
}

export const myProfile = async(req,res,next)=>{
    try {
        let _id = req._id

        let result = await WebUser.findById(_id)

        res.json({
            success:true,
            message:"profile read succesfully",
            data:result
        })
        // res.json("*****************")
    } catch (error) {
        res.json({
            success:false,
            message:"unable to read profile"
        })
    }
}

export const updateProfile = async (req,res,next)=>{

    try {
        let _id = req._id
        let data = req.body

        let result = await WebUser.findByIdAndUpdate(_id, data, {new:true})

        res.json({
            success:true,
            message:"profile updated successful",
            data:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}