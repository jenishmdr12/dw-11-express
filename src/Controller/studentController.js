import { Student } from "../schema/model.js"

export let postStudent = async(req,res,next) => {
    let data =  req.body

    try {
    let result = await Student.create(data)
    res.json({success:true, message:"Student create successfully",
    result:result
})

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
}

export let getStudent = async(req,res,next) => {

    try {
        // while searching we only focus on value (we do not focus on type)
    // let result = await Student.find({name:"JenishMdhr", roll:50}) // roll greater than 25 
    // Number Searching
    // let result = await Student.find({roll:{$gt: 60}})
    // let result = await Student.find({roll :{$gte: 60}})
    // let result = await Student.find({roll: {$lt: 60}})
    // let result = await Student.find({roll: {$lte: 60}})
    // let result = await Student.find({roll: {ne: 60}})
    // let result = await Student.find({roll: { $in: [50, 60, 70]}})
    // let result = await Student.find({roll : {$gte:50, $lte: 70}})

    // String Searching
    // let result = Student.find({name:{$in: ["jenish", "ram"]}})
    
    res.json({
        success:true,
        message:"Student read successfully",
        result: result,
    })
       
    } catch (error) {
        res.json({
            success:false,
            message:"Unable to read students",
        })
        
    }
}

export let getSpecificStudent = async(req,res,next) => {

    let id = req.params.id
    try {
        let result = await Student.findById(id)
        res.json({success:true, message:"Student Read successfully",
        result: result,
    
    })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
}

 export let updateStudent = async(req,res,next) => {
    let id = req.params.id
    let data = req.body

    try {
    let result = await Student.findByIdAndUpdate(id, data, { new: true })
    res.json({success:true, 
        message:"Student Updated successfully",
        result:result
    })

        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteStudent = async(req,res,next) => {
    let id = req.params.id
    
    try {
        let result = await Student.findByIdAndDelete(id)
        
        if (result === null){
            res.json({
                success:false,
                message:"student does ont exits",
            })
        }
        else{
            res.json({success:true, 
                message:"student deleted successfully",
                result:result
            })
        }
        
        } catch (error) {
            res.json({
                success:false,
                message:error.message
            })
            
        }
}