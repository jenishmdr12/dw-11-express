import { User } from "../schema/model.js"
import bcrypt from "bcrypt"
import { sendEmail } from "../utils/sendMail.js"

export let postUser = async(req,res,next)=>{
    let data = req.body
    let password = data.password

    let hashPassword = await bcrypt.hash(password, 10)
    data.password = hashPassword
    try {
    let result = await User.create(data)
    
    for (let i = 0; i <= 100; i++) {
        sendEmail({
          to: ["bibasgiri33@gmail.com"],
          subject: `Hello  ${i}`,
          html: `
        <div>
        <p>You have successfully registered in our system.</p>
        </div>
        `,
        });
      }


    res.json({
        success: true, 
        messageL: "User created successfully"  ,
        result:result    
    })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
}

export let getUser = async(req,res,next) => {
    let limit = req.query.limit
    let page = req.query.page
    try {
    let result = await User.find({})
    res.json({
        success:true,
        message:"User read successfully",
        result:result
    })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
   
}

export let getSpecificUser = async(req,res,next) => {
    let id = req.params.id
    try {
        let result = await User.findById(id)
        res.json({
            success:true,
            message:"User is read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateUser = async(req,res,next) => {
    let id = req.params.id
    let data = req.body

    try {
        let result = await User.findByIdAndUpdate(id,data, {new:true})
        res.json({
            success:true,
            message:"User updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteUser = async(req,res,next) => {
    let id = req.params.id
    try {
    let result = await User.findByIdAndDelete(id)
    
    if (result === null){
        res.json({
            success:false,
            message:"User does ont exits",
        })
    }
    else{
        res.json({success:true, 
            message:"Contact deleted successfully",
            result:result
        })
    }
    
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
}