import { config } from "dotenv";

config()
export let port = process.env.PORT
export let mongoUrl = process.env.MONGO_URL
export let dbUrl = process.env.DB_URL

export let password = process.env.PASSWORD
export let email = process.env.EMAIL
export let secretKey = process.env.SECRET_KEY
export let user = process.env.user
export let pass = process.env.pass

