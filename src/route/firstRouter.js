import { Router } from "express";

export let firstRouter = Router()

firstRouter.route("/")
.post(
   (req, res, next)=>{
   console.log("i am middleware 1")
   req.name="jenish"
   next()
   },
   (req,res,next)=>{
      console.log(req.name)
      console.log("i am middleware 2")
   }
)

.get((req, res, next)=>{
   console.log('i am middleware 1')
   let error = new Error("my error")
   next(error) 

},(err, req, res, next)=>{
   console.log('i am middleware 2')
   next()
})

.delete((req, res, next)=>{
   res.json("bike Delete")
})

.patch((req, res, next)=>{
   let data =req.body
   //console.log(data)
   res.json(data)
})