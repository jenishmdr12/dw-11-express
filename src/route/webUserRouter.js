import { Router } from "express"
import { createWebUser, getWebUser, loginUser, myProfile, updateProfile, verifyEmail } from "../Controller/webUserController.js"
import { secretKey } from "../constan.js"
import jwt from "jsonwebtoken"
import isAuthenticated from "../../middleware/isAuthenticated.js"
export let webUserRouter = Router()
webUserRouter.route("/")
.post(createWebUser)
.get(getWebUser)

webUserRouter.route("/verify-email").patch(verifyEmail)
webUserRouter.route("/login").post(loginUser)
webUserRouter.route("/my-profile").get(isAuthenticated, myProfile)
webUserRouter.route("/update-profile").patch(isAuthenticated, updateProfile)


  