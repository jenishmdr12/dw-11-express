import { Router } from "express"
import { deleteProduct, getProduct, getSpecificProduct, postProduct, updateProduct } from "../Controller/productController.js"

export let productRouter = Router()
productRouter.route("/")
.post(postProduct)
.get(getProduct)

productRouter.route("/:id")
.get(getSpecificProduct)
.patch(updateProduct)
.delete(deleteProduct)