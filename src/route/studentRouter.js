import { Router } from "express";
import { Student } from "../schema/model.js";
import { deleteStudent, getSpecificStudent, getStudent, postStudent, updateStudent } from "../Controller/studentController.js";


export let studentRouter = Router()
studentRouter.route('/') //localhost:8000/students/

.post(postStudent)
.get(getStudent)

studentRouter.route('/:id') //localhost:8000/students/anything

.get(getSpecificStudent)
.patch(updateStudent)

.delete(deleteStudent)
