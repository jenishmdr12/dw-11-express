import { Router } from "express";
import { College } from "../schema/model.js";

export let collegeRouter = Router()
collegeRouter.route("/")

.post(async(req,res,next) => {
    let data =  req.body

    try {
    let result = await College.create(data)
    res.json({success:true, message:"college create successfully"})

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
})
.get(async(req,res,next) => {

    try {
    let result = await College.find({})
    res.json({
        success:true,
        message:"College read successfully",
        result: result,
    })
       
    } catch (error) {
        res.json({
            success:false,
            message:"Unable to read colleges",
        })
        
    }
})

collegeRouter.route('/:id') //localhost:8000/colleges/anything

.get(async(req,res,next) => {

    let id = req.params.id

    try {
        let result = await College.findById(id)
        res.json({success:true, message:"College Read successfully",
        result: result,
    
    })
        

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
})
.patch(async(req,res,next) => {
    let id = req.params.id
    let data = req.body

    try {
    let result = await College.findByIdAndUpdate(id, data, { new: true })
    res.json({success:true, 
        message:"College Updated successfully",
        result:result
    })

        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
})
.delete((req,res,next) => {
    
    res.json({success:true, message:"College deleted successfully"})
})
