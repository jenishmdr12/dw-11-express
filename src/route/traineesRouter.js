import { Router } from "express";
import { Trainee } from "../schema/model.js";
import { deleteTrainee, getSpecificTrainee, getTrainee, postTrainee, updateTrainee } from "../Controller/traineeController.js";

export let traineesRouter = Router()
traineesRouter.route('/')

.post(postTrainee)
.get(getTrainee)

traineesRouter.route('/:id')

.get(getSpecificTrainee)
.patch(updateTrainee)
.delete(deleteTrainee)
