import { Router } from "express"
import { deleteUser, getSpecificUser, getUser, postUser, updateUser } from "../Controller/userController.js"

export let userRouter = Router()
userRouter.route("/")
.post(postUser)
.get(getUser)

userRouter.route("/:id")
.get(getSpecificUser)
.patch(updateUser)
.delete(deleteUser)