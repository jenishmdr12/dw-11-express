import { Router } from "express"
import { deleteReview, getReview, getSpecificReview, postReview, updateReview } from "../Controller/reviewController.js"

export let reviewRouter = Router()
reviewRouter.route('/') //localhost:8000/students/

.post(postReview)
.get(getReview)

reviewRouter.route('/:id') //localhost:8000/students/anything

.get(getSpecificReview)
.patch(updateReview)

.delete(deleteReview)
