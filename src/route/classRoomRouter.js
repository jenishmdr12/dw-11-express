import { Router } from "express";
import { deleteClassRoom, getClassRoom, getSpecificClassRoom, postClassRoom, updateClassRoom } from "../Controller/classRoomController.js";

export let classRoomRouter = Router()
classRoomRouter.route('/')

.post(postClassRoom)
.get(getClassRoom)

classRoomRouter.route('/:id')
.get(getSpecificClassRoom)
.patch(updateClassRoom)
.delete(deleteClassRoom)