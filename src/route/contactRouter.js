import { Router } from "express"
import { deleteContact, getContact, getSpecificContact, postContact, updateContact } from "../Controller/contactController.js"

export let contactRouter = Router()
contactRouter.route("/")
.post(postContact)
.get(getContact)

contactRouter.route("/:id")
.get(getSpecificContact)
.patch(updateContact)
.delete(deleteContact)