import { Schema } from "mongoose";

 
export let traineeSchema = Schema({
    name: {
        type : String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    faculty: {
        type: String,
        required: true,
    }
})

