import { Schema } from "mongoose";

export let userSchema = Schema(
    {
    profileImage:{
        type:String,
        required:true
    },
    name: {
        type : String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required:true,
        unique: true,
    },
    password: {
        type:String,
        required: [true, "password field is required"],
        // validate: (value) => {
        //     let isValidPassword = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[a-zA-Z0-9!@#$%^&*()_+]{8,15}$/.test(value)
        //     if (!isValidPassword) {
        //         throw new Error("password must valid")
        //     }
        // }
    },
    phoneNumber: {
        type: Number,
        validate: (value)=> {

            let strPhoneNumber = String(value)
            if (strPhoneNumber.length === 10){

            }else {
                throw new Error("phoneNumber must be exact 10 character long")
            }
        },
        trim: true,
        required: [true, "phoneNumber field is required"],
    },
},
{
    timestamps: true
})
