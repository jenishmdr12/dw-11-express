import { Schema } from "mongoose";

export let webUserSchema = Schema({
    fullName: {
        type : String,
        required: [true, "fullName field is required"],
    },
    email: {
        type: String,
        unique: true,
        required:[true,"password field is required"],
    },
    password: {
        type:String,
        required: [true, "password field is required"],
        // validate: (value) => {
        //     let isValidPassword = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[a-zA-Z0-9!@#$%^&*()_+]{8,15}$/.test(value)
        //     if (!isValidPassword) {
        //         throw new Error("password must valid")
        //     }
        // }
    },
    dob:{
        type:Date,
        required: [true, "dob field is required"],
    },
    gender:{
        type:String,
        required: [true, "gender field is required"]
    },
    role:{
        type:String,
        required: [true, "role field is required"]
    },
    isVerifiedEmail:{
        type:Boolean,
        required:[true, "isVerifiedEmail is required"]
    }
},{timestamp:true})
