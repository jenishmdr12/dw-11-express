import { Schema } from "mongoose";

export let studentSchema = Schema({
    name: {
        type: String,
        required: [true, "name field is required"],
        // lowercase: true,
        // uppercase: true,
        // trim: true,
        minLength: [3, "name must be at least 3 character long."],
        maxLength: [30, "name must be at least 30 character long."],

        
        validate: (value) => {

            let onlyAlphabet = /^[a-zA-Z]+$/.test(value)

            if (onlyAlphabet) {
            }else {
                throw new Error("name field must be only alphabet")
            }
        }
    },
    phoneNumber: {
        type: Number,
        validate: (value)=> {

            let strPhoneNumber = String(value)
            if (strPhoneNumber.length === 10){

            }else {
                throw new Error("phoneNumber must be exact 10 character long")
            }
        },
        trim: true,
        required: [true, "phoneNumber field is required"],
    },
    password: {
        type:String,
        required: [true, "password field is required"],
        validate: (value) => {
            let isValidPassword = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[a-zA-Z0-9!@#$%^&*()_+]{8,15}$/.test(value)
            if (!isValidPassword) {
                throw new Error("password must valid")
            }
        }
    },
    roll: {
        type:Number,
        required:[true, "roll field is required"],
        min : [50, "roll must be greater than or equal to 50"],
        max: [100, "roll must be less than or equal to 100"],
    },
    isMarried: {
        type: Boolean,
        required: [true, "isMarried field is required"],
    },
    spouseName: {
        type: String,
        required: [true, "spouseName field is required"],
    },
    email:{
        type: String,
        required:[true, "email field is required"],
        validate: (value) => {
            let isValidEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)
            if (!isValidEmail) {
                throw new Error("email must valid")
            }
        },
        unique: true,
    },
    gender:{
        type:String,
        // default: "Male",
        validate: (value)=>{
            if (value ==="male" || value ==="female" || value === "other"){
            }else {
                let error = new error("gender must be either male, female, other")
                throw error
            }

        },
        required:[true, "gender field is required"],
    },
    dob:{
        type:Date,
        required:[true, "dob field is required"]
    },
    location:{
        country:{
            type:String,
            required:[true, "Country field is required"]
        },
        exactLocation:{
            type:String,
            required:[true, "exactLocation is required"]
        }
    },
    favTeacher:[
        {
            type:String,
        }
    ],
    favSubject:[
        {
            bookName:{
                type:String,
            },
            bookAuthor:{
                type:String,
            },
        },
    ],
})
 