import { Schema } from "mongoose";

export let classRoomSchema = Schema({
    name: {
        type : String,
        required: [true, " name field is required"]
    },
    numberOfBench: {
        type: Number ,
        required: [true, "numberOfBench field is required"]
    },
    hasTv: [
        {
        type: String,
        }
]
})