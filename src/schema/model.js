import { model } from "mongoose";
import { studentSchema } from "./studentSchema.js";
import { teacherSchema } from "./teacherSchema.js";
import { collegeSchema } from "./collegeSchema.js";
import { traineeSchema } from "./traineeSchema.js";
import { classRoomSchema } from "./classRoomSchema.js";
import { contactSchema } from "./contactSchema.js";
import { productSchema } from "./productSchema.js";
import { userSchema } from "./userSchema.js";
import { reviewSchema } from "./reviewSchema.js";
import { webUserSchema } from "./webUserSchema.js";

export let Student = model("Student", studentSchema)
export let Teacher = model("Teacher", teacherSchema)
export let Trainee = model("Trainee", traineeSchema)
export let College =  model("College", collegeSchema)
export let ClassRoom = model("ClassRoom", classRoomSchema)
export let Review = model("Review", reviewSchema)
export let Contact = model("Contact", contactSchema)
export let Product = model("Product", productSchema)
export let User = model("User", userSchema)
export let WebUser = model("WebUser", webUserSchema)


